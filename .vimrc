vim9script
se nu
# se relativenumber
se hidden
se hlsearch
se shortmess+=c
se nocp
se signcolumn=yes
se noinfercase
se t_Co=256
se laststatus=2
se ts=4
se cursorline
se rtp+=~/.cache/dein/repos/github.com/Shougo/dein.vim
var mapleader = " "
se bg=light
se title
se go=a
se mouse=a
nnoremap c "_c
se wildmode=longest,list,full
se termguicolors
se autoindent
se smartindent
se encoding=utf-8
se splitbelow splitright
g:pymode_python = 'python3'
call dein#begin('~/.cache/dein/')
call dein#add('~/.cache/dein/repos/github.com/Shougo/dein.vim')
call dein#add('vim-scripts/Auto-Pairs')
call dein#add('tpope/vim-surround')
call dein#add('voldikss/vim-floaterm')
call dein#add('ptzz/lf.vim')
call dein#add('itchyny/lightline.vim')
call dein#add('haya14busa/dein-command.vim')
call dein#add('sainnhe/everforest')
call dein#add( 'iamcco/mathjax-support-for-mkdp', 
	\{'on_ft': 'md'})
call dein#add('iamcco/markdown-preview.vim', 
	\{'on_ft': 'md'})
call dein#add('arcticicestudio/nord-vim')
call dein#add('liuchengxu/vim-which-key')
call dein#add('davidhalter/jedi-vim',
						\ {on_event: 'InsertEnter'})
call dein#end()
if dein#check_install()
	call dein#install()
endif
g:lightline = {
			\ 'colorscheme': 'nord',
			\ }
g:deoplete#enable_at_startup = 1
g:everforest_better_performance = 1
g:everforest_background = 'soft'
syn on
filetype plugin indent on
colorscheme nord
