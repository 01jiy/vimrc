vim9script
se nu
# se relativenumber
se hidden
se hlsearch
se shortmess+=c
se nocp
se signcolumn=yes
syntax on
filetype plugin indent on
se noinfercase
se completeopt-=preview
se completeopt+=menuone,noselect
se t_Co=256
se laststatus=2
se encoding=utf-8
se cursorline
g:UltiSnipsExpandTrigger = "ok"
g:UltiSnipsJumpForwardTrigger = "<c-b>"
g:UltiSnipsJumpBackwardTrigger = "<c-z>"
g:UltiSnipsEditSpli = "vertical"
se rtp+=~/.local/share/dein/repos/github.com/Shougo/dein.vim
var mapleader = " "
se background=dark
se termguicolors
se autoindent
se smartindent
