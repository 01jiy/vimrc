vim9script
call dein#begin('~/.local/share/dein/')
call dein#add('~/.local/share/dein/repos/github.com/Shougo/dein.vim')
call dein#add('vim-scripts/Auto-Pairs')
call dein#add('tpope/vim-surround')
call dein#add('voldikss/vim-floaterm')
call dein#add('ptzz/lf.vim')
call dein#add('itchyny/lightline.vim')
call dein#add('honza/vim-snippets')
call dein#add('haya14busa/dein-command.vim')
call dein#add('neoclide/coc.nvim', {'build': 'yarn install'})
call dein#add('sainnhe/everforest')
call dein#end()
if dein#check_install()
	call dein#install()
endif
g:lightline = {
			\ 'colorscheme': 'everforest',
			\ }
g:deoplete#enable_at_startup = 1
g:everforest_better_performance = 1
g:everforest_background = 'soft'
colorscheme everforest
